const vscode = require('vscode');

/**
 * @param {vscode.ExtensionContext} context
 */
function activate(context) {
  const emmet = vscode.workspace.getConfiguration('emmet');
  const includeLanguages = emmet.get('includeLanguages');
  const updated = Object.assign(includeLanguages, { javascript: 'html'});
  emmet.update('includeLanguages', updated, true);
}
exports.activate = activate;

function deactivate() {}

module.exports = {
	activate,
	deactivate
}
